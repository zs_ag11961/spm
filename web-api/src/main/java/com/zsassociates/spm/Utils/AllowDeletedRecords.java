package com.zsassociates.spm.Utils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface AllowDeletedRecords {
    boolean value() default false;
}
