package com.zsassociates.spm.Utils;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

public class HttpUtility {

    public static Response makeHttpRequest(
            String url,
            HashMap<String, String> requestHeaders,
            String requestBody,
            String method
    ) throws IOException {

        RequestMehtods requestMehtods = getRequestMehtod(method);
        HttpURLConnection httpURLConnection = createHttpConnection(url,requestMehtods);
        setRequestHeaders(requestHeaders,httpURLConnection);
        setRequestBody(requestBody,httpURLConnection,requestMehtods);
        return getResponse(httpURLConnection);
    }

    private static HttpURLConnection createHttpConnection(String url, RequestMehtods method) throws IOException {
        URL uRL = new URL(url);
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = (HttpURLConnection) uRL.openConnection();
            httpURLConnection.setRequestMethod(method.toString());
            httpURLConnection.setConnectTimeout(120000);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        return httpURLConnection;
    }

    private static RequestMehtods getRequestMehtod(String method)
    {
        RequestMehtods requestMehtod = null;
        try {
            requestMehtod = RequestMehtods.valueOf(method.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new BadRequestException("Invalid resquest method");
        }
        return requestMehtod;
    }

    private static void setRequestHeaders(HashMap<String, String> requestHeaders,HttpURLConnection httpURLConnection) {
        if (requestHeaders != null) {

            for (String key : requestHeaders.keySet()) {
                httpURLConnection.addRequestProperty(key, requestHeaders.get(key));
            }
        }

    }

    private static void setRequestBody(String requestBody,HttpURLConnection httpURLConnection,RequestMehtods requestMehtod) throws IOException {
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        OutputStream outputStream = null;
        BufferedWriter bufferedWriter = null;
        if (!requestMehtod.equals(RequestMehtods.GET) && (requestBody != null)) {
            try {
                outputStream = httpURLConnection.getOutputStream();
                bufferedWriter = new BufferedWriter(
                        new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
                bufferedWriter.write(requestBody);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
            } catch (Exception ex) {
                // Log Error
                System.out.println(ex.getMessage());
                throw ex;
            } finally {
                if (bufferedWriter != null) bufferedWriter.close();
                if (outputStream != null) outputStream.close();
            }
        }
    }

    private static Response getResponse(HttpURLConnection httpURLConnection) throws IOException {

        String responseBody;
        StringBuffer responseBuffer = null;
        int responseCode = httpURLConnection.getResponseCode();

        BufferedReader bufferdReader = null;
        if (responseCode == 200 || responseCode == 204) {
            bufferdReader = new BufferedReader(
                    new InputStreamReader(httpURLConnection.getInputStream()));
        } else {
            bufferdReader = new BufferedReader(
                    new InputStreamReader(httpURLConnection.getErrorStream()));
        }
        String inputLine;
        try {
            responseBuffer = new StringBuffer();
            while ((inputLine = bufferdReader.readLine()) != null) {
                responseBuffer.append(inputLine);
            }
            responseBody = responseBuffer.toString();
        } catch (Exception ex) {
            // Log Error
            System.out.println(ex.getMessage());
            throw ex;
        } finally {
            bufferdReader.close();
            responseBuffer.setLength(0);
            httpURLConnection.disconnect();
        }

        return Response.status(responseCode).entity(responseBody).build();
    }

    private enum RequestMehtods {
        PUT,
        POST,
        GET,
        DELETE
    }

}