package com.zsassociates.spm.Utils;

import com.zsassociates.aws.runtime.proxy.JWTRequestFilter;

import java.util.ArrayList;

/**
 * This class defines the List of URL's that needs to be excluded from authentication
 */
public class PAExclusionListRetriever implements JWTRequestFilter.IExclusionListRetriever {
    @Override
    public ArrayList<String> getExcludedBasePaths() {
        ArrayList<String> excludedPaths = new ArrayList<> ();
        excludedPaths.add ( "isalive" );
        excludedPaths.add ( "hello" );
        excludedPaths.add ( "authenticate/accesstoken" );
        return excludedPaths;
    }
}
