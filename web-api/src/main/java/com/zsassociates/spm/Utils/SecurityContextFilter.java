package com.zsassociates.spm.Utils;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;

public class SecurityContextFilter implements ContainerResponseFilter {

    private  final String X_Frame_Options = "X-Frame-Options";
    private  final String Cache_Control = "Cache-Control";
    private  final String No_Cache = "no-cache,no-store,must-revalidate";
    private  final String Pragma = "Pragma";
    private  final String X_Content_Type_Options = "X-Content-Type-Options";
    private  final String NOSNIFF = "nosniff";
    private  final String SAMEORIGIN = "SAMEORIGIN";


    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        setSecurityHeaders (responseContext);
    }

    private void setSecurityHeaders(ContainerResponseContext responseContext)
    {
        responseContext.getHeaders().putSingle(X_Frame_Options,SAMEORIGIN);
        responseContext.getHeaders().putSingle(Cache_Control,No_Cache);
        responseContext.getHeaders().putSingle(Pragma,No_Cache);
        responseContext.getHeaders().putSingle(X_Content_Type_Options,NOSNIFF);
    }

}
