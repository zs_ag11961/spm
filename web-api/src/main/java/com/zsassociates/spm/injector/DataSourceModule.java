package com.zsassociates.spm.injector;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.zsassociates.spm.IDao.ITerritoryTypeDao;
import com.zsassociates.spm.Utils.PAExclusionListRetriever;
import com.zsassociates.aws.runtime.proxy.DefaultInfraResourcePingerResolver;
import com.zsassociates.aws.runtime.proxy.InfraResourcePingerResolver;
import com.zsassociates.aws.runtime.proxy.JWTRequestFilter;
import com.zsassociates.spm.dao.TerritoryTypeDao;
import com.zsassociates.spm.handlers.Handlers.TerritoryTypeHandler;
import com.zsassociates.spm.handlers.IHandlers.ITerritoryTypeHandler;

public class DataSourceModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(JWTRequestFilter.IExclusionListRetriever.class).to(PAExclusionListRetriever.class);
        //bind(JWTRequestFilter.PgConnectionSessionVariablesProvider.class).to(PGSessionProvider.class);
        bind(InfraResourcePingerResolver.class).to(DefaultInfraResourcePingerResolver.class);
        registerRowMapper();
        registerDao();
        registerHandlers();

    }





    private void registerDao() {
        bind(new TypeLiteral<ITerritoryTypeDao>() {
        }).to(TerritoryTypeDao.class);
        bind(TerritoryTypeDao.class);
        bind(TerritoryTypeDao.TerritoryTypeRowMapper.class);
    }

    private void registerHandlers() {
        bind(new TypeLiteral<ITerritoryTypeHandler>() {
        }).to(TerritoryTypeHandler.class);
        bind(TerritoryTypeHandler.class);
    }

    private void registerRowMapper() {

    }
}