package com.zsassociates.spm.controllers;


import com.zsassociates.spm.handlers.IHandlers.ITerritoryTypeHandler;
import com.zsassociates.spm.models.TerritoryType;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;


@Path("/hello")
public class HelloWorldController //extends BaseResource {
{
    @Inject
    ITerritoryTypeHandler territoryTypeHandler;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getTerritories() {
        //List<TerritoryType> result = territoryTypeHandler.getTerritories(1);
        //return   result.toString();
        return  "Hello World !! " +  new Date().toString();
    }
}
