package com.zsassociates.spm.controllers;

import com.zsassociates.aws.runtime.local.ZSHttpServer;
public class TestLocalServer extends ZSHttpServer {

    @Override
    public int getPort() {
        return 9092;

    }

    public static void main(String[] args) {

        TestLocalServer testLocalServer = new TestLocalServer();
        testLocalServer.init();
    }
}
