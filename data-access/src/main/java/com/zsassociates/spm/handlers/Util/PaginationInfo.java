package com.zsassociates.spm.handlers.Util;

public class PaginationInfo {

    private long limit;
    private long offset;
    private long totalCount;
    private SortDirection sortDirection;

    public PaginationInfo(long limit, long offset) {
        this.limit = limit;
        this.offset = offset < 0 ? 0 : offset;
    }

    public PaginationInfo(long limit, long offset, SortDirection sortDirection) {
        this.limit = limit;
        this.offset = offset < 0 ? 0 : offset;
        this.sortDirection = sortDirection;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public long getLimit() {
        return limit;
    }

    public void setLimit(long limit) {
        this.limit = limit;
    }

    public long getTotalCount() {

        return totalCount;
    }

    public long getOffset() {
        return offset;
    }

    String getPaginationQuery() {
        if (limit < 0)
            return "";
        return " OFFSET :offset LIMIT :limit";
    }

    String getNextPageQueryString() {
        if ((limit >= totalCount || (offset + limit) >= totalCount || limit < 0) && totalCount != -1)
            return "";
        else
            return getPaginationQueryString(limit, limit + offset);
    }

    String getPreviousPageQueryString() {

        if (offset == 0 || limit < 0) {
            return "";
        } else {
            long index = (offset - limit) < 0 ? 0 : (offset - limit);
            return getPaginationQueryString(limit, index);
        }
    }

    private String getPaginationQueryString(long limit, long offset) {
        return ("limit=" + limit + "&offset=" + offset);
    }

    public SortDirection getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(SortDirection sortDirection) {
        this.sortDirection = sortDirection;
    }
}
