package com.zsassociates.spm.handlers.Util;

import java.util.TreeMap;

public class TableMappers {

	public static abstract class TableMapper {

		public TreeMap<String, Mapper> mapper;

		public String getFieldNameWithAlias(String fieldName) {

			return String.format("%s.%s", mapper.get(fieldName).tableAlias, mapper.get(fieldName).mappingColumnName);
		}

		public void addMapping(String key, Mapper mapper) {
			this.mapper.put(key.toLowerCase(), mapper);
		}

	}
}
