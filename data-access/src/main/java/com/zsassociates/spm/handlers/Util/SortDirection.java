package com.zsassociates.spm.handlers.Util;

public enum SortDirection {
    asc,
    desc;

    public static SortDirection getSortDirection(String order) {
        try {
            return valueOf(order.toLowerCase());
        } catch (Exception e) {
            return null;
        }
    }
}

