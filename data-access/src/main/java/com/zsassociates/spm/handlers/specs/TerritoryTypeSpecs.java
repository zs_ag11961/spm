package com.zsassociates.spm.handlers.specs;

public class TerritoryTypeSpecs {


    public static class GetTerritoryTypes  extends Specs.BaseSpec {

        private long territoryTypeId;

        public GetTerritoryTypes() {

        }

        public long getTerritoryTypeId() {
            return territoryTypeId;
        }
    }
}