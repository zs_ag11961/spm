package com.zsassociates.spm.handlers.Util;

public class Mapper{
    private Object value;
    String mappingColumnName;
    public String tableAlias;

    public Mapper(String mappingColumnName, String tableAlias)
    {
        this.mappingColumnName = mappingColumnName;
        this.tableAlias = tableAlias;
    }

    void setValue(Object value)
    {
        this.value = value;
    }

    public Object getValue()
    {
        return this.value;
    }
}