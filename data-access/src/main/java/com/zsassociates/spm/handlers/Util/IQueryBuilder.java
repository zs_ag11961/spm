package com.zsassociates.spm.handlers.Util;

import java.io.UnsupportedEncodingException;

public interface IQueryBuilder {

    String queryStringWithoutPagination() throws UnsupportedEncodingException;

    String getNextURL() throws UnsupportedEncodingException;

    String getPreviousURL() throws UnsupportedEncodingException;

    void setTotalCount(long count);

    String getPaginationQuery();

    PaginationInfo getPaginationDetails();

    String getFilterQuery(TableMappers.TableMapper mapper, boolean hasWhereClause) throws Exception;

    String getSortQuery(TableMappers.TableMapper mapper);

    void setLoggedInUserInfo(String userName);

    void setLoggedInUserId(long userId);

    String getUserFilter(TableMappers.TableMapper mapper, boolean isJoinQuery) throws Exception;

    String getQuery(TableMappers.TableMapper mapper, boolean hasWhereClause) throws Exception;

    String getSearchString(SearchOperations operation);

    boolean isSearchApplied();

    boolean isFilterPartExist(String filterPart);

    long getLoggedinUserId();

    boolean getAddcompetitor();

    String getEntitySubType();
}
