package com.zsassociates.spm.handlers.Util;

import com.zsassociates.spm.models.BaseEntity;

import javax.ws.rs.BadRequestException;
import java.util.ArrayList;
import java.util.List;

public class MutateUtility {

    public static long[] getLongArrayfromString(String values,String delimiter){
        long[] idsArray=null;
        if (values!=null && !values.isEmpty () ){
        try {

            String[] ids = values.split(delimiter);
            idsArray = new long[ids.length];
            for(int i=0;i<ids.length;i++) {
                idsArray[i] = Long.parseLong (ids[i]);
            }
        }catch (Exception e){
            throw new BadRequestException( "Invalid ids Filter" );
        }
        }
        return idsArray;
    }

    // Create the list of entity ids
    public static List<Long> getListOfEntityIds(List<BaseEntity> entities)
    {
        List<Long> entityIds = new ArrayList<>();

        for (BaseEntity entity : entities)
        {
            entityIds.add(entity.getId());
        }

        return entityIds;
    }
}
