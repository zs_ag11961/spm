package com.zsassociates.spm.handlers.Util;

import javax.ws.rs.BadRequestException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

;

class SortInfo {

    private final String commaSeparator = ",";
    private SortDirection direction;
    private String sortString;

    public SortInfo(String sortString) {
        this.sortString = sortString;
        this.direction = SortDirection.asc;
    }

    public String getSortQuery(TableMappers.TableMapper mapper) {
        if ((sortString == null) || sortString.trim().isEmpty())
            return "";

        String[] sortParameters = sortString.split(Constants.KeyValueSeparator);
        if (sortParameters.length > 2)
            throw new BadRequestException("Invalid Sort Info");

        if (sortParameters.length == 2) {
            try {
                direction = SortDirection.valueOf(sortParameters[1].toLowerCase());
            } catch (IllegalArgumentException e) {
                throw new BadRequestException("Invalid sort direction");
            }
        }

        String sortFields = getSortOrderFields(sortParameters[0].toLowerCase(), mapper, direction);


        return String.format(" Order By %s", sortFields);
    }

    public String getSortQueryString() throws UnsupportedEncodingException {
        if (sortString.equals(""))
            return "";
        return Constants.sortQueryString.concat("=" + URLEncoder.encode(sortString, Constants.ENCODETYPE));
    }

    private String getSortOrderFields(String sortString, TableMappers.TableMapper mapper, SortDirection direction) {
        String[] sortFields = sortString.split(Constants.fieldSeparatorForSort);
        String sortFieldsString = "";
        boolean isFirst = true;
        for (String field : sortFields) {
            if (!mapper.mapper.containsKey(field))
                throw new BadRequestException("Invalid sort key");

            if (isFirst) {
                sortFieldsString = sortFieldsString.concat(mapper.getFieldNameWithAlias(field));
            } else {
                sortFieldsString = sortFieldsString.concat(commaSeparator).concat(mapper.getFieldNameWithAlias(field));
            }

            sortFieldsString = sortFieldsString.concat(" ").concat(direction.toString());
            isFirst = false;
        }
        return sortFieldsString;
    }
}

