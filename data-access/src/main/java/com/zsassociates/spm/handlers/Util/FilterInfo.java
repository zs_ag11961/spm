package com.zsassociates.spm.handlers.Util;


import com.zsassociates.spm.exceptions.BadRequestException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

;


class FilterInfo {

    private String filterString;
    private String[] filtersArray;
    private String loggedInUser;
    private boolean applyUserFilter;
    private StringBuilder sbl;
    private String filterStringCopy;
    private long userId;

    FilterInfo(final String filterString, Boolean applyUserFilter) {
        this.filterString = filterString.trim();
        this.applyUserFilter = applyUserFilter;
        filterStringCopy = this.filterString;
    }

    public String getUserFilter(TableMappers.TableMapper mapper, boolean hasWhereClause) {
        String clauseQuery = "";
        sbl = new StringBuilder(clauseQuery);
        getFilter(getUserFilterExpression(), hasWhereClause, true, mapper);

        return sbl.toString();
    }

    String getClause(TableMappers.TableMapper mapper, boolean hasWhereClause) {

        if (filterString.equals("") || filterString.isEmpty()) {
            return "";
        }

        filtersArray = this.filterString.split(Constants.filtersSeparator);
        String clauseQuery = "";
        sbl = new StringBuilder(clauseQuery);

        for (int i = 0; i < filtersArray.length; i++) {
            boolean isFirstFilter = (i == 0);
            getFilter(filtersArray[i], hasWhereClause, isFirstFilter, mapper);
        }

        return sbl.toString();
    }

    private void getFilter(String expression, boolean hasWhereClause, boolean isFirstFilter, TableMappers.TableMapper mapper) {

        if (expression.equals("")) {
            return;
        }

        String[] detail = expression.split(Constants.KeyValueSeparator);

        if (detail.length != 3) {
            throw new BadRequestException("Invalid filter passed");
        }

        if (isFirstFilter && !hasWhereClause) {
            sbl.append(" WHERE ");
        } else {
            sbl.append(" AND ");
        }

        sbl.append(createExpression(mapper, detail[0], detail[2], detail[1]));
    }

    private String GetSqlString(String operation, String _fieldValue, String _fieldName) {
        _fieldValue = getParameterisedName(_fieldValue);

        switch (operation) {
            case Constants.EqualsOperatorSymbol: {
                return String.format("%s = %s", _fieldName, _fieldValue);
            }
            case Constants.NotEqualsOperatorSymbol: {
                return String.format("(%s != %s or %s is null)", _fieldName, _fieldValue, _fieldName);
            }
            case Constants.InOperatorSymbol:{
                return String.format("%s in (%s)", _fieldName, _fieldValue, _fieldName);
            }
            case Constants.AnyOperatorSymbol:{
                return String.format("%s = any(%s)", _fieldName, _fieldValue, _fieldName);
            }
            default: {
                throw new BadRequestException("Non Supported Filter Operation");
            }
        }
    }

    private String getParameterisedName(String name) {
        return ":".concat(name);
    }

    private String createExpression(TableMappers.TableMapper mapper, String fieldName, Object value, String operation) {

        fieldName = fieldName.toLowerCase();

        if (!mapper.mapper.containsKey(fieldName)) {
            throw new BadRequestException("Invalid filter passed");
        }

        mapper.mapper.get(fieldName).setValue(value);

        return GetSqlString(operation, fieldName, mapper.getFieldNameWithAlias(fieldName));
    }

    public String getFilterQueryString() throws UnsupportedEncodingException {
        if (filterStringCopy.equals(""))
            return "";
        return Constants.filterQueryString.concat("=" + URLEncoder.encode(filterStringCopy, Constants.ENCODETYPE));
    }

    public void setLoggedInUserInfo(String userName) {

        loggedInUser = userName;

        if (applyUserFilter) {
            filterString = getUserFilterExpression().concat(Constants.pipeSeparator).concat(filterString);
        }
    }

    private String getUserFilterExpression() {
        return Constants.userNameAttributeName.concat(Constants.KeyValueSeparator).concat(Constants.EqualsOperatorSymbol.concat(Constants.KeyValueSeparator).concat(loggedInUser));
    }

    public boolean isFilterPartExist(String filterPart) {

        if (filtersArray == null) {
            return false;
        }

        for (String filterString : filtersArray) {

            String[] detail = filterString.split(Constants.KeyValueSeparator);

            if (detail[0].equalsIgnoreCase(filterPart)) {
                return true;
            }
        }

        return false;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
