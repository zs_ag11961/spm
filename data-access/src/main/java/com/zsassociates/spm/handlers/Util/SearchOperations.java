package com.zsassociates.spm.handlers.Util;

public enum SearchOperations {
    StartsWith,
    EndsWith,
    Contains,
    ContainsStartWith,
    Exactmatch,
    AdvanceSearch,
}
