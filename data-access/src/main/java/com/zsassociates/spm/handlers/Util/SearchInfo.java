package com.zsassociates.spm.handlers.Util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

public class SearchInfo {

    private String searchString;

    private String likeEscapeChar = "%";

    private boolean isSearchApplied = false;

    public SearchInfo(String searchString) {
        this.searchString = searchString;

        if (!this.searchString.trim ().isEmpty ())
        {
            isSearchApplied = true;
        }
    }

    public boolean isSearchApplied() {
        return isSearchApplied;
    }

    public String getSearchString(SearchOperations operation) {
        switch (operation) {
            case Exactmatch:
                return searchString;
            case Contains:
                return likeEscapeChar.concat ( searchString ).concat ( likeEscapeChar );
            case EndsWith:
                return likeEscapeChar.concat ( searchString );
            case StartsWith:
                return searchString.concat ( likeEscapeChar );
            case ContainsStartWith:
                return likeEscapeChar.concat ( " ".concat ( searchString ) ).concat ( likeEscapeChar );
            case AdvanceSearch:
                return getAdvanceSearchString(searchString);

            default:
                return likeEscapeChar.concat ( searchString ).concat ( likeEscapeChar );
        }
    }

    private String getAdvanceSearchString(String searchString) {

        String processedString = "";
        if (!searchString.isEmpty()) {
            List<String> listOfString = Arrays.asList(searchString.split(" "));

            int index = 0;
            for (index = 0; index < listOfString.size() - 1; index++) {

                if (!listOfString.get(index).trim().isEmpty()) {

                    processedString = processedString.concat(String.format("%s:* &", listOfString.get(index)));
                }

            }

            processedString = processedString.concat(String.format(" %s:*", listOfString.get(index)));

        }
        return processedString;

    }

    public String getSearchQueryString() throws UnsupportedEncodingException {
        if (!isSearchApplied)
        {
            return "";
        }

        return Constants.searchQueryString.concat ( "=" + URLEncoder.encode(searchString, Constants.ENCODETYPE) );
    }
}
