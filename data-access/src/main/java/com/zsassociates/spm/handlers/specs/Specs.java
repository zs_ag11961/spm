package com.zsassociates.spm.handlers.specs;

import com.zsassociates.spm.dao.Queries;
import com.zsassociates.spm.handlers.Util.Constants;
import com.zsassociates.spm.handlers.Util.Filters.Filter;
import com.zsassociates.spm.handlers.Util.MutateUtility;

public class Specs {

    public static abstract class BaseSpec {

        public String getSql() {
            return Queries.getQuery(this.getClass().getSimpleName()).getSql();
        }

        public String getCountSql() {
            return String.format("Select count(1) from (%s) as totalCount", String.format(Queries.getQuery(this.getClass().getSimpleName()).getSql()));
        }
    }

    public static abstract class FilterSpec extends BaseSpec {

        private final long userId;
        protected Filter filter;
        private String query;
        private boolean hasWhereClause;

        protected FilterSpec(Filter filter, boolean hasWhereClause) throws Exception {
            this.filter = filter;
            this.hasWhereClause = hasWhereClause;
            this.query = filter.getQuery(this.hasWhereClause);
            this.userId = filter.getUserId();
        }

        protected FilterSpec(Filter filter, boolean hasWhereClause, long userId) throws Exception {
            this.filter = filter;
            this.hasWhereClause = hasWhereClause;
            this.query = filter.getQuery(this.hasWhereClause);
            this.userId = userId;
        }
        public String getSql() {
            String queryKey = this.getClass().getSimpleName();
            Queries.Query query = Queries.getQuery(queryKey);

            return query.getSql().concat(this.query);
        }

        public long[] getIdsArrayFromFilter(String values){
            return MutateUtility.getLongArrayfromString (values,",");
        }

        public String getCountSql() {
            String queryKey = this.getClass().getSimpleName();
            String queryfilter = "";
            Queries.Query query = Queries.getQuery(queryKey);

            try {
                queryfilter = filter.getFilterQuery(hasWhereClause);
            } catch (Exception e) {
            }

            return String.format("Select count(1) from (%s) as totalCount", String.format(query.getSql(), queryfilter, ""));
        }

        public long getLimit() {

            return filter.getLimit();
        }

        public long getoffset() {

            return filter.getOffSet();
        }

        public long getUserId() {
            return this.userId;
        }

        public String getusername() {
            return (String) filter.getAttributeValue("userName");
        }
    }
}
