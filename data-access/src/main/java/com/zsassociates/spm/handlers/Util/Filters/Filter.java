package com.zsassociates.spm.handlers.Util.Filters;

import com.zsassociates.spm.handlers.Util.Mapper;
import com.zsassociates.spm.handlers.Util.*;
import com.zsassociates.spm.handlers.Util.TableMappers;

import java.io.UnsupportedEncodingException;
import java.util.List;

public abstract class Filter {

    TableMappers.TableMapper tableMapper;
    public IQueryBuilder queryBuilder;

    void setQueryBuilder(IQueryBuilder queryBuilder) {

        this.queryBuilder = queryBuilder;
    }

    public String getQuery(boolean hasWhereClause) throws Exception {
        return queryBuilder.getQuery(tableMapper, hasWhereClause);
    }

    public String getFilterQuery(boolean hasWhereClause) throws Exception {
        return queryBuilder.getFilterQuery(tableMapper, hasWhereClause);
    }

    public boolean isFilterPartExist(String filterPart) {
        return queryBuilder.isFilterPartExist(filterPart);
    }

    public String getSortQuery() {
        return queryBuilder.getSortQuery(tableMapper);
    }

    public String getPaginationQuuery() {
        return queryBuilder.getPaginationQuery();
    }

    public Object getAttributeValue(String attributeName) {
        return tableMapper.mapper.get(attributeName.toLowerCase()).getValue();
    }

    public String getUserFilter(boolean hasWhereClause) throws Exception {
        return queryBuilder.getUserFilter(tableMapper, hasWhereClause);
    }

    public long getLimit() {

        return queryBuilder.getPaginationDetails().getLimit();
    }

    public void setLimit(long limit) {
        queryBuilder.getPaginationDetails().setLimit(limit);
    }

    public long getOffSet() {
        return queryBuilder.getPaginationDetails().getOffset();
    }

    public String getnextURL(String baseURL, String requestURL) throws UnsupportedEncodingException {

        String paramURL = queryBuilder.getNextURL();

        if (paramURL == null || paramURL.isEmpty()) {
            return "";
        }

        return getFinalURL(baseURL, requestURL, paramURL);
    }

    public String getPreviousURL(String baseURL, String requestURL) throws UnsupportedEncodingException {

        String paramURL = queryBuilder.getPreviousURL();

        if (paramURL == null || paramURL.isEmpty()) {
            return "";
        }

        return getFinalURL(baseURL, requestURL, paramURL);
    }

    public long getTotalCount() {
        return queryBuilder.getPaginationDetails().getTotalCount();
    }

    public void setTotalCount(long count) {
        queryBuilder.setTotalCount(count);
    }

    private String getFinalURL(String baseURL, String requestURL, String paramURL) {
        if (paramURL.equals("")) {
            return "";
        } else {
            return baseURL.concat(requestURL).concat("?").concat(paramURL);
        }
    }

    public void addDynamicColumnMapping(List<String> dynamicColumnsNames, String tableAlias) {
        for (String columnName : dynamicColumnsNames) {
            tableMapper.addMapping(columnName, new Mapper(columnName, tableAlias));
        }
    }

    public String getSearchString(SearchOperations operations) {

        return queryBuilder.getSearchString(operations);
    }

    public boolean isSearchApplied() {
        return queryBuilder.isSearchApplied();
    }

    public long getUserId() {
        return queryBuilder.getLoggedinUserId();
    }

    public String queryStringWithoutPagination() throws UnsupportedEncodingException {
        return queryBuilder.queryStringWithoutPagination();
    }

    public IQueryBuilder getQueryBuilder() {
        return queryBuilder;
    }
}
