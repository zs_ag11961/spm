package com.zsassociates.spm.handlers.Util;

import com.zsassociates.spm.models.Util.*;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

public class UrlConstructor {

    private UrlConstructor() {
    }

    public static String constructApiUrl(List<String> urlComponents, List<String> queryParameters, List<UrlQueryParameter> filterParameters, boolean includeBaseUrl) {

        // Constructing the url
        StringBuilder urlBuilder = new StringBuilder();

        if(includeBaseUrl) {
            urlBuilder.append(BaseApiUrlProvider.GetBaseApiUrl());
        }

        if (urlComponents != null && !urlComponents.isEmpty()) {
            for (String urlComponent : urlComponents) {
                urlBuilder.append(Constants.urlComponentSeparator).append(urlComponent);
            }
        }

        // Construct filter
        if (filterParameters != null && !filterParameters.isEmpty()) {
            String filterQueryString = constructFilterParameter(filterParameters);
            if (queryParameters == null) {
                queryParameters = Arrays.asList(filterQueryString);
            } else {
                queryParameters.add(filterQueryString);
            }
        }

        // Adding the query paramaters
        if (queryParameters != null && !queryParameters.isEmpty() && !queryParameters.get(0).equals("")) {
            constructQueryParameters(urlBuilder, queryParameters);
        }

        return urlBuilder.toString();
    }

    private static StringBuilder constructQueryParameters(StringBuilder urlBuilder, List<String> queryParameters) {
        boolean isFirst = true;
        urlBuilder.append(Constants.urlQueryStringStarter);

        for (String queryParameter : queryParameters) {
            if (isFirst) {
                urlBuilder.append(queryParameter);
                isFirst = false;
            } else {
                urlBuilder.append(Constants.urlQueryStringSeparator).append(queryParameter);
            }
        }
        return urlBuilder;
    }

    private static String constructFilterParameter(List<UrlQueryParameter> filterParameters) {

        StringBuilder filterBuilder = new StringBuilder();
        filterBuilder.append(Constants.filterQueryString).append("=");

        StringBuilder filterValueBuilder = new StringBuilder();

        boolean isFirst = true;
        if (filterParameters != null && !filterParameters.isEmpty()) {
            for (UrlQueryParameter filterParameter : filterParameters) {
                if (isFirst) {
                    filterValueBuilder.append(filterParameter.getKeyOperatorValue());
                    isFirst = false;
                } else {
                    filterValueBuilder.append(Constants.pipeSeparator).append(filterParameter.getKeyOperatorValue());
                }
            }
            // Encode filter value
            String encodedFilterValues = encodeUrlString(filterValueBuilder.toString());
            filterBuilder.append(encodedFilterValues);
        }

        return filterBuilder.toString();
    }

    private static String encodeUrlString(String urlString) {
        try {
            urlString = URLEncoder.encode(urlString, Constants.ENCODETYPE);
        } catch (UnsupportedEncodingException ex) {
            // We are eating the exception because we don't expect to get this.
            // Logging the exception message

        }
        return urlString;
    }
}