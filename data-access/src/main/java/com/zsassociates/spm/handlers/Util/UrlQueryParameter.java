package com.zsassociates.spm.handlers.Util;

public class UrlQueryParameter {

    private String key;
    private String value;
    private String operator;

    public UrlQueryParameter(String key, String value, String operator) {
        this.key = key;
        this.value = value;
        this.operator = operator;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getKeyOperatorValue() {
        return String.format("%s%s%s%s%s", this.getKey(), Constants.KeyValueSeparator, this.getOperator(), Constants.KeyValueSeparator, this.getValue());
    }
}