package com.zsassociates.spm.handlers.Util;
import org.glassfish.jersey.internal.util.collection.StringKeyIgnoreCaseMultivaluedMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.io.UnsupportedEncodingException;

public class QueryBuilder implements IQueryBuilder {

    private PaginationInfo paginationInfo;

    private FilterInfo filterInfo;

    private SortInfo sortInfo;

    private SearchInfo searchInfo;

    private MultivaluedMap<String, String> queryParams;

    private StringBuilder queryParamBuilder;

    public QueryBuilder(UriInfo uriInfo) {
        setUp (uriInfo,applyUserFilter ());
    }

    public QueryBuilder(UriInfo uriInfo,boolean applyUserFilter) {
        setUp (uriInfo,applyUserFilter);
    }

    private void setUp(UriInfo uriInfo, boolean applyUserFilter) {
        this.queryParams = getQueryParameters(uriInfo.getQueryParameters());
        paginationInfo = new PaginationInfo(getLimit(), getOffset());
        this.filterInfo = new FilterInfo(getFilterQueryString(),applyUserFilter);
        this.sortInfo = new SortInfo(getSortQueryString());
        searchInfo = new SearchInfo ( getSearchQueryString () );
    }

    private MultivaluedMap<String, String> getQueryParameters(MultivaluedMap<String, String> params) {
        MultivaluedMap<String, String> queryParams = new StringKeyIgnoreCaseMultivaluedMap<>();
        for (Object key : params.keySet()) {
            queryParams.put((String) key, params.get(key));
        }
        return queryParams;
    }

    private long getLimit() {
        if (queryParams.containsKey ( Constants.limitQueryString ))
            return Long.parseLong ( queryParams.getFirst ( Constants.limitQueryString ) );

        return 10;
    }

    private long getOffset() {
        if (queryParams.containsKey ( Constants.offsetQueryString ))
            return Long.parseLong ( queryParams.getFirst ( Constants.offsetQueryString ) );

        return 0;
    }

    private String getFilterQueryString() {
        if (queryParams.containsKey ( Constants.filterQueryString ))
            return queryParams.getFirst ( Constants.filterQueryString );

        return "";
    }

    private boolean applyUserFilter() {
        if(queryParams == null)
            return true;
        if (queryParams.containsKey ( Constants.userFilterQueryString ))
            return Boolean.parseBoolean ( queryParams.getFirst ( Constants.userFilterQueryString ) );
        return true;
    }

    private String getSortQueryString() {
        if (queryParams.containsKey(Constants.sortQueryString)) {
            return queryParams.getFirst ( Constants.sortQueryString );
        }

        return "";
    }

    public boolean getAddcompetitor() {

        if (queryParams.containsKey(Constants.addcompetitorQueryStrint)) {
            return Boolean.valueOf(queryParams.getFirst(Constants.addcompetitorQueryStrint));
        }

        return false;
    }

    public String getEntitySubType(){
        return String.valueOf(queryParams.getFirst((Constants.entitySubTypeQueryString)));
    }

    private String getSearchQueryString() {
        if (queryParams.containsKey ( Constants.searchQueryString )) {
            return queryParams.getFirst ( Constants.searchQueryString );
        }
        return "";
    }

    public PaginationInfo getPaginationDetails() {
        return paginationInfo;
    }

    public String getQuery(TableMappers.TableMapper mapper, boolean hasWhereClause) {
        return filterInfo.getClause(mapper, hasWhereClause).concat(sortInfo.getSortQuery(mapper)).concat(paginationInfo.getPaginationQuery());
    }

    public String getFilterQuery(TableMappers.TableMapper mapper, boolean hasWhereClause) {
        return filterInfo.getClause(mapper, hasWhereClause);
    }

    public boolean isFilterPartExist(String filterPart) {
        return filterInfo.isFilterPartExist(filterPart);
    }

    @Override
    public long getLoggedinUserId() {
        return filterInfo.getUserId();
    }

    public String getPaginationQuery() {
        return paginationInfo.getPaginationQuery();
    }

    public String getSortQuery(TableMappers.TableMapper mapper) {
        return sortInfo.getSortQuery(mapper);
    }

    public void setLoggedInUserInfo(String userName) {
        filterInfo.setLoggedInUserInfo(userName);
    }

    @Override
    public void setLoggedInUserId(long userId) {
        filterInfo.setUserId(userId);
    }

    public String getUserFilter(TableMappers.TableMapper mapper, boolean isJoinQuery) {
        return filterInfo.getUserFilter(mapper, isJoinQuery);
    }

    public String queryStringWithoutPagination() throws UnsupportedEncodingException {

        queryParamBuilder = new StringBuilder();
        addParametersToQueryString(filterInfo.getFilterQueryString());
        addParametersToQueryString(sortInfo.getSortQueryString());
        addParametersToQueryString(searchInfo.getSearchQueryString());

        return queryParamBuilder.toString();
    }

    public String getNextURL() throws UnsupportedEncodingException {

        String nextUrl = paginationInfo.getNextPageQueryString();

        if (nextUrl == null || nextUrl.isEmpty()) {
            return null;
        }

        queryParamBuilder = new StringBuilder();
        addParametersToQueryString(filterInfo.getFilterQueryString());
        addParametersToQueryString(sortInfo.getSortQueryString());
        addParametersToQueryString(searchInfo.getSearchQueryString());
        addParametersToQueryString(getaddcompetitorQueryString());
        addParametersToQueryString((getEntitySubTypeQueryString()));
        addParametersToQueryString(nextUrl);

        return queryParamBuilder.toString();
    }

    public String getPreviousURL() throws UnsupportedEncodingException {

        String previousUrl = paginationInfo.getPreviousPageQueryString();

        if (previousUrl == null || previousUrl.isEmpty()) {
            return null;
        }

        queryParamBuilder = new StringBuilder();

        addParametersToQueryString(filterInfo.getFilterQueryString());
        addParametersToQueryString(sortInfo.getSortQueryString());
        addParametersToQueryString ( searchInfo.getSearchQueryString () );
        addParametersToQueryString(getaddcompetitorQueryString());
        addParametersToQueryString((getEntitySubTypeQueryString()));
        addParametersToQueryString(previousUrl);

        return queryParamBuilder.toString();
    }

    public void setTotalCount(long count) {
        paginationInfo.setTotalCount(count);
    }

    public String getSearchString(SearchOperations operation) {
        return  "";
        //return searchInfo.getSearchString((SearchOperation)operation);
    }

    private String getaddcompetitorQueryString() {

        if (!queryParams.containsKey(Constants.addcompetitorQueryStrint)) {
            return "";
        }

        return String.format("%s=%s", Constants.addcompetitorQueryStrint, queryParams.getFirst(Constants.addcompetitorQueryStrint));
    }

    private String getEntitySubTypeQueryString(){
        if (!queryParams.containsKey(Constants.entitySubTypeQueryString)) {
            return "";
        }
        return String.format("%s=%s", Constants.entitySubTypeQueryString, queryParams.getFirst(Constants.entitySubTypeQueryString));
    }

    public boolean isSearchApplied() {
        return searchInfo.isSearchApplied ();
    }

    private void addParametersToQueryString(String queryParam) {

        if (queryParam.equals("")) {
            return;
        }
        if(queryParamBuilder!=null) {
            if (queryParamBuilder.toString().equals("")) {
                queryParamBuilder.append(queryParam);
            } else {
                queryParamBuilder.append("&" + queryParam); }
        }
    }
}
