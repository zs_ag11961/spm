package com.zsassociates.spm.handlers.Util;

public class Constants {

    //region GenericConstants
    public static final String filtersSeparator = "\\|";
    public static final String pipeSeparator = "|";
    public static final String KeyValueSeparator = "~";
    public static final String EqualsOperatorSymbol = "::";
    public static final String AssignimentOperatorSymbol = "=";
    public static final String NotEqualsOperatorSymbol = "!:";
    public static final String InOperatorSymbol = "in";
    public static final String AnyOperatorSymbol = "any";
    public static final String tableAliasForUsersTable = "users";
    public static final String tableAliasForEntityTypeTable = "entitytype";
    public static final String tableAliasForUserinsightpreferenceType = "uipreferencesType";
    public static final String tableAliasForUserinsightpreferenceValue = "uipreferencesValues";
    public static final String tableAliasForInsightAssociationTable = "ia";
    public static final String filterQueryString = "$filter";
    public static final String sortQueryString = "$sort";
    public static final String addcompetitorQueryStrint = "addcompetitor";
    public static final String entitySubTypeQueryString = "entitySubType";
    public static final String limitQueryString = "limit";
    public static final String offsetQueryString = "offset";
    public static final String userFilterQueryString = "applyUserFilter";
    public static final String tableAliasForCustomersTable = "customers";
    public static final String getTableAliasForOrgUnitsTable = "orgunits";
    public static final String searchQueryString = "$search";
    public static final String tableAliasForCategoriesTable = "categories";
    public static final String getTableAliasForActionTypeTable = "actiontype";
    public static final String getTableAliasForSuggestionActionMapping = "SuggestionActionMapping";
    public static final String fieldSeparatorForSort = "\\|";
    public static final String urlComponentSeparator = "/";
    public static final String urlQueryStringStarter = "?";
    public static final String urlQueryStringSeparator = "&";
    public static final String tableAliasForInsights = "insights";
    public static final String InsightCountPlaceholder = "InsightsCount";

    public static final String contentDispositionheaderKey = "Content-Disposition";
    public static final String contentTypeHeaderkey = "Content-Type";
    public static final String userinsightpreferenceTypeDeleted = "deleted";
    public static final String userinsightpreferenceValueDeleted = "deleted";
    public static final String multiValueSeparator = ",";
    public static final String usersuggestionpreferenceTypeDismissed = "dismissed";
    public static final String usersuggestionpreferenceValueDismissed = "dismissed";
    public static final String usersuggestionpreferenceTypeCompleted = "completed";
    public static final String usersuggestionpreferenceValueCompleted = "completed";

    public static final String pagenumberQueryString = "pagenumber";
    public static final String offsetsQueryString = "offsets";
    public static final String additionalOffsetsQueryParametersString = "&pagenumber=%s&offsets=%s";
    //endregion


    //region UserConstants
    public static final String userNameAttributeName = "userName";
    public static final String userNameColumnName = "username";
    //endregion

    //region InsightsConstant
    public static final String insightobservationColumnName = "insightobservation";
    public static final String insightIDColumnName = "insightid";
    public static final String insightIDsAttributeName = "insightids";
    //endregion


    //region EntityTypeConstants
    public static final String EntityTypeNameAttributeName = "EntityType";
    public static final String EntityTypeNameColumnName = "type";
    //endregion


    //region UserInsightPreferenceValueConstant
    public static final String preferenceValueAttributeName = "preferenceValue";
    public static final String preferenceValueColumnName = "preferenceValue";
    //endregion


    public static final String userInsightPrefrenceTempTableAlias = "TempUserInsightsPrefPivot";

    //region UserInsightPreferenceTypeConstant
    public static final String preferenceTypeAttributeName = "preferenceType";
    public static final String preferenceTypeColumnName = "preferenceType";
    //endregion


    //region InsightAssociationsConstants
    public static final String entityIdAttributeName = "entityId";
    public static final String entityIdColumnName = "entityId";
    //endregion


    //region CustomersConstants
    public static final String customerFirstNameAttributeName = "firstname";
    public static final String customerMiddleNameAttributeName = "middlename";
    public static final String customerLastNameAttributeName = "lastname";
    public static final String customerNameAttributeName = "customerName";
    public static final String customerNameColumnName = "customername";
    public static final String customerTypeIdAttributeName = "customerTypeId";
    public static final String customerTypeIdColumnName = "customerTypeId";
    public static final String customerAddressAttributeName = "address";
    public static final String customerAddressColumnName = "address";
    public static final String customeridColumnName = "customerid";
    public static final String customeridsAttributeName = "customerids";
    //endregion


    //region usercustomerpreferences
    public static final String tableAliasForuserorgunitpreferences = "uoup";
    public static final String tableAliasForusercustomerpreferences = "ucp";
    public static final String preferencetypeColumnName = "preferencetype";
    //endregion

    //region OrgUnitConstants
    public static final String orgunitNameAttributeName = "orgunitname";
    public static final String orgunitNameColumnName = "orgunitname";
    public static final String orgunitParentidAttributeName = "orgunitparentid";
    public static final String orgunitParentidColumnName = "orgunitParentid";
    public static final String orgunitlevelidAttributeName = "orgunitlevelid";
    public static final String orgunitlevelidColumnName = "orgunitlevelid";
    public static final String orgunitidColumnName = "orgunitid";
    public static final String orgunitidsAttributeName = "orgunitids";

    public static final String productIdColumnName = "productid";
    public static final String productIdAttributeName = "productid";
    public static final String getTableAliasForProductsTable = "products";
    public static final String tableAliasForProductPlanNumericMetricTable = "ocpppc";
    public static final String customeridAttributeName = "customerid";
    public static final String orgunitidAttributeName = "orgunitid";


    public static final String ACTIVITY = "activity";
    public static final String DETAILS = "details";
    //endregion

    //region ActionType
    public static final String ActionTypeIdAttributeName = "actiontypeid";
    public static final String ActionTypeIdColumnName = "Actiontypeid";
    //endregion

    //region CategoryConstants
    public static final String categoryIdAttributeName = "categoryid";
    public static final String categoryIdColumnName = "categoryid";
    public static final String categoryParentIdColumnName = "parentid";
    //endregion

    //region URL Constants

    public static final String ENTITY = "entity";
    public static final String CUSTOMER = "customer";
    public static final String ENCODETYPE = "UTF-8";
    public static final String ORGUNIT = "orgunit";
    public static final String INSIGHTS = "insights";
    public static final String SALESFORCE = "salesforce";
    public static final String AFFILIATIONS = "affiliations";
    public static final String ASSOCIATIONS = "associations";
    public static final String PAYERS = "payers";
    public static final String AFFINITY = "affinity";
    public static final String SUGGESTION = "suggestion";
    public static final String SuggestionCountPlaceholder = "SuggestionsCount";
    public static final String PLAN = "plan";
    public static final String COUNT = "count";
    public static final String PRODUCT = "product";
    public static final String NOPARENT = "noparent";

    public static final String preferencetype_deleted = "deleted";
    public static final String preferencevalue_deleted = "deleted";
    //endregion

    //region entity metric

    public static final String ENTITYRELATEDINFOALERTS = "Insights & Suggestions";
    public static final String ENTITYRELATEDAFFILIATIONS = "Affiliations";
    public static final String ENTITYRELATEDASSOCIATIONS = "Associations";
    public static final String ENTITYRELATEDPAYERS = "Payers";
    public static final String ENTITYRELATEDAFFINITY = "Affinity";
    //endregion

    // suggestion

    public static final String tableAliasForSuggestion = "suggestion";
    public static final String suggestiontextColumnName = "suggestiontext";
    public static final String userSuggestionsPrefrenceTempTableAlias = "TempUserSuggestionsPrefPivot";
    public static final String suggestionIDColumnName = "SuggestionId";
    public static final String suggestionIDsAttributeName = "suggestionIds";

    public static final String SALESFORCEID_PLACEHOLDER = "{salesforceid}";

    public static final String allowDeletedRecords = "pa.isdeleted.disabled";
    public static final String allowDeletedRecordsQueryParameter = "allowdeleted";

    public static final String invalidQueryParameters = "Invalid query parameters.";

    public static final String dateFormat = "MM/DD/YYYY";
    public static final String personalizeUrlTemplate = "/entity/%s/salesforce/%s/types/follow";

    public static final String apnsCertificatePath = "apns.CertificatePath";
    public static final String apnsCertificatePassword = "apns.CertificatePassword";
    public static final String apnsServer = "apns.Server";
    public static final String apnsDefaultTopic = "apns.DefaultTopic";
}
