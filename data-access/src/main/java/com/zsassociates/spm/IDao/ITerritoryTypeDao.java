package com.zsassociates.spm.IDao;

import com.zsassociates.spm.dao.IDAO;
import com.zsassociates.spm.models.TerritoryType;

import java.util.List;

public interface ITerritoryTypeDao extends IDAO<TerritoryType> {
     List<TerritoryType> getTerritories(long userId);
}


