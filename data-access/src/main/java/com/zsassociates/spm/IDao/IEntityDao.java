package com.zsassociates.spm.IDao;

import com.zsassociates.spm.dao.IDAO;
import com.zsassociates.spm.handlers.specs.Specs;
import com.zsassociates.spm.models.Entity;

import java.util.List;

public interface IEntityDao extends IDAO<Entity> {
    List<Entity> getEntities(Specs.BaseSpec spec);
}
