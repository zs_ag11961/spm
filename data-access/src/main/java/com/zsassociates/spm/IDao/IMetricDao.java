package com.zsassociates.spm.IDao;

import com.zsassociates.spm.dao.IDAO;
import com.zsassociates.spm.models.BaseEntity;

public interface IMetricDao extends IDAO<BaseEntity> {
}
