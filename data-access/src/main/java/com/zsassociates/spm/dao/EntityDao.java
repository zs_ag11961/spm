package com.zsassociates.spm.dao;
import com.zsassociates.spm.IDao.IEntityDao;
import com.zsassociates.spm.handlers.specs.Specs;
import com.zsassociates.spm.models.Entity;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.datasource.AbstractDataSource;
;

import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class EntityDao extends BaseDao<Entity> implements IEntityDao {

    @Inject
    public EntityDao(final AbstractDataSource ds, final EntityDao.EntityRowMapper rowMapper) {
        super(ds, rowMapper);
    }

    @Override
    public List<Entity> getEntities(Specs.BaseSpec spec) {
        return null;
    }

    public static class EntityRowMapper implements RowMapper<Entity> {

      //  @Nullable
        @Override
        public Entity mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Entity(rs.getLong("entityId"),
                    rs.getString("type")
                    //comment
            );
        }
    }

    public static class EntitySalesforceRowMapper implements RowMapper<Entity> {

      //  @Nullable
        @Override
        public Entity mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Entity(rs.getLong("entityId"),
                    rs.getString("type"),
                    rs.getLong("salesforceId")
            );
        }
    }

    // TODO: If needed, clean up once we finalize on the approach for salesforce id in territory url
    public List<Entity> getEntitiesWithSalesforce(Specs.BaseSpec spec)
    {
        SqlParameterSource params = new BeanPropertySqlParameterSource(spec);
        return namedTemplate.query (spec.getSql(), params, new EntityDao.EntitySalesforceRowMapper());
    }

}
