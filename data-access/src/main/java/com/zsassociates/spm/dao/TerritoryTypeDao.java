package com.zsassociates.spm.dao;

//import org.springframework.lang.Nullable;
import com.zsassociates.spm.IDao.ITerritoryTypeDao;
import com.zsassociates.spm.models.TerritoryType;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.AbstractDataSource;

import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public class TerritoryTypeDao extends BaseDao<TerritoryType> implements ITerritoryTypeDao {

    @Inject
    public TerritoryTypeDao(final AbstractDataSource ds, final TerritoryTypeDao.TerritoryTypeRowMapper rowMapper) {
        super(ds, rowMapper);
    }

    @Override
    public List<TerritoryType> getTerritories(long userId) {
        return null;
    }

    public static class TerritoryTypeRowMapper implements RowMapper<TerritoryType> {

       // @Nullable
        @Override
        public TerritoryType mapRow(ResultSet resultSet, int rowNum) throws SQLException {
            return new TerritoryType(
                    resultSet.getLong("id"),
                    resultSet.getString("name"));
        }
    }

}

