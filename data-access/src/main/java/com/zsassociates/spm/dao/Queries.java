package com.zsassociates.spm.dao;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigObject;
import com.typesafe.config.ConfigValue;

import java.util.HashMap;
import java.util.Map;

public class Queries {

    private static final Map<String, Query> queries = createQueriesMap();

    private static Map<String, Query> createQueriesMap() {

        Map<String, Query> queries = new HashMap<>();

        Config queriesFromConfig = ConfigFactory.load("app-queries/queries.conf");

        for (Map.Entry<String, ConfigValue> query : queriesFromConfig.root ().entrySet ()) {
            if (query.getKey ().contains ( "-create" )) {
                queries.put ( query.getKey (), createInsertQuery ( (ConfigObject) query.getValue () ) );
            } else {
                queries.put ( query.getKey (),
                        new Query ( (query.getValue ().unwrapped ().toString ()) ) );
            }
        }
        return queries;
    }

    private static InsertQuery createInsertQuery(ConfigObject queryObject) {
        String insertSql = queryObject.get ( "sql" ).unwrapped ().toString ();
        String[] keyColumnNames = queryObject.toConfig ().getStringList ( "keyColumnNames" )
                .stream ()
                .toArray ( String[]::new );
        return new InsertQuery ( insertSql, keyColumnNames );
    }

    public static Query getQuery(String queryName) {
        return queries.get(queryName);
    }

    static InsertQuery getInsertQuery(String queryName) {
        return (InsertQuery) queries.get(queryName);
    }

    public static class Query {
        private final String sql;

        Query(final String sql) {
            this.sql = sql;
        }

        public String getSql() {
            return sql;
        }
    }

    static class InsertQuery extends Query {

        private final String[] keyColumnNames;

        InsertQuery(String sql, String[] keyColumnNames) {
            super(sql);
            this.keyColumnNames = keyColumnNames;
        }

        String[] getKeyColumnNames() {
            return keyColumnNames;
        }
    }
}
