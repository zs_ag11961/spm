package com.zsassociates.spm.models.Logs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zsassociates.spm.models.BaseEntity;
import com.zsassociates.spm.models.Enums.ApiType;

import java.sql.Timestamp;

@JsonIgnoreProperties({"selfUrl"})
public class InsightsLog extends BaseEntity {
    private long insightId;
    private long userId;
    private Timestamp apiExecutionTime;
    private ApiType apiType;

    public InsightsLog(long insightId, long userId, ApiType apiType, Timestamp apiExecutionTime){
        super();
        this.insightId = insightId;
        this.userId = userId;
        this.apiType = apiType;
        this.apiExecutionTime = apiExecutionTime;
    }

    public long getInsightId() {
        return insightId;
    }

    public void setInsightId(long insightId) {
        this.insightId = insightId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Timestamp getApiExecutionTime() {
        return apiExecutionTime;
    }

    public int getApiType() {
        return apiType.apiTypeValue;
    }

    public void setApiType(ApiType apiType) {
        this.apiType = apiType;
    }

    public void setApiExecutionTime(Timestamp apiExecutionTime) {
        this.apiExecutionTime = apiExecutionTime;
    }

    @Override
    public String createSelfURL() {
        return null;
    }
}

