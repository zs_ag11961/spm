package com.zsassociates.spm.models;

public class TerritoryType extends  BaseEntity{

    private   String name;

    public TerritoryType(long id,String name){
        super(id);
        this.name = name;
    }

    public  TerritoryType(){

    }

    public  void setName(String name) {
        this.name = name;
    }

    public  void setId(long id){
        this.id = id;
    }

    @Override
    public String createSelfURL(){
        return String.format ( "%s/%s/%s/%s", getBaseUrl (), "categories", getId (), "" );
    }

}
