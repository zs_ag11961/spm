package com.zsassociates.spm.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zsassociates.spm.models.Util.BaseApiUrlProvider;

/**
 * The base entity.
 */
@JsonIgnoreProperties({"baseUrl"})
public abstract class BaseEntity {

    public String selfUrl;
    protected long id;
    private String baseUrl;

    public BaseEntity() {
        this.baseUrl = BaseApiUrlProvider.GetBaseApiUrl();
        selfUrl = createSelfURL();
    }

    public BaseEntity(long id) {
        this.id = id;
        this.baseUrl = BaseApiUrlProvider.GetBaseApiUrl();
        selfUrl = createSelfURL();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    protected String getBaseUrl() {
        return baseUrl;
    }

    protected void SetSelfUrl() {
        selfUrl = createSelfURL();
    }

    abstract public String createSelfURL();
}

