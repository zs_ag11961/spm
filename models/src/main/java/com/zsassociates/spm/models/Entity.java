package com.zsassociates.spm.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"salesforceId"})
public class Entity extends BaseEntity {

    private String entityType;
    private long salesforceId;

    public Entity() {

    }

    public Entity(long entityId, String entityType) {
        super(entityId);
        this.entityType = entityType;
        selfUrl = createSelfURL();
    }

    public Entity(long id, String entityType, long salesforceId) {
        super(id);
        this.entityType = entityType;
        this.salesforceId = salesforceId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public long getSalesforceId() {
        return salesforceId;
    }

    public void setSalesforceId(long salesforceId) {
        this.salesforceId = salesforceId;
    }

    @Override
    public String createSelfURL() {
        return String.format("%s/%s/%s/%s", getBaseUrl(), "entity", this.entityType, String.valueOf(getId()));
    }
}
