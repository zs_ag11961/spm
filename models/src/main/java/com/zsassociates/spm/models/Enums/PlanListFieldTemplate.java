package com.zsassociates.spm.models.Enums;

import java.util.Arrays;

public enum PlanListFieldTemplate {

    // Note: Do not change the enum values

    status(0),
    favourability(1),
    tier(2);


    public final int value;

    PlanListFieldTemplate(int planListFieldTemplate) {
        this.value = planListFieldTemplate;
    }

    public static PlanListFieldTemplate getPlanListFieldTemplateValue(int templateConfigurationId) {
        return Arrays.stream(values()).filter(type -> type.value == templateConfigurationId).findFirst().orElse(getDefaultConfiguration());
    }

    public static PlanListFieldTemplate getDefaultConfiguration()
    {
        return PlanListFieldTemplate.status;
    }
}
