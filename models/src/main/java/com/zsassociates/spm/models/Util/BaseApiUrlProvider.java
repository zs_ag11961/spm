package com.zsassociates.spm.models.Util;

import com.typesafe.config.Config;
import com.zsassociates.aws.configuration.ConfigurationManager;

public class BaseApiUrlProvider {

    private static String baseUrl;

    private BaseApiUrlProvider() {
        baseUrl = "";
    }

    public static String GetBaseApiUrl() {
     if (baseUrl==null || baseUrl.isEmpty ())
     {
         Config appConfig = ConfigurationManager.getAppConfig ();
         String domain = ConfigurationManager.getStringOrElse ( appConfig, "domain", "" );
         baseUrl = String.format ( "%s://%s", "https", domain );
     }
        return baseUrl;
    }
}
