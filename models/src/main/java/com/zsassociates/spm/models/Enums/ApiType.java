package com.zsassociates.spm.models.Enums;

public enum ApiType{
    insightList(1),
    insightDetail(2);

    public final int apiTypeValue;

    ApiType(int apiTypeValue) {
        this.apiTypeValue = apiTypeValue;
    }
}
