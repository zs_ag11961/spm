package com.zsassociates.spm.models.SystemConfiguration;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zsassociates.spm.models.BaseEntity;
import java.util.Date;


@JsonIgnoreProperties({"selfUrl"})
public class ConfigurationSetting extends BaseEntity {

    private Object key;

    private Object value;

    private Date LastUpdatedTime;

    private String LastUpdatedBy;


    public ConfigurationSetting(Object key, Object value, Date LastUpdatedTime, String LastUpdatedBy) {
        this.key = key;
        this.value = value;
        this.LastUpdatedBy = LastUpdatedBy;
        this.LastUpdatedTime = LastUpdatedTime;
    }

    public ConfigurationSetting()
    {

    }

    public Object getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }


    public void setKey(Object key) {
        this.key = key;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Date getLastUpdatedTime() {
        return LastUpdatedTime;
    }

    public String getLastUpdatedBy() {
        return LastUpdatedBy;
    }

    @Override
    public String createSelfURL() {
        return null;
    }
}
