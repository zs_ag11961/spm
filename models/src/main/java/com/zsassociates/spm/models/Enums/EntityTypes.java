package com.zsassociates.spm.models.Enums;

public enum EntityTypes {
    customer,
    product,
    salesforce,
    orgunit
}
