package com.zsassociates.spm.models.Enums;

import java.util.Arrays;

public enum EntityListTemplateConfiguration {

    // Note: Do not change the enum values

    FollowUnfollow(0),
    Metric(1),
    FollowUnfollowAndMetric(2);


    public final int value;

    EntityListTemplateConfiguration(int templateConfiguration) {
        this.value = templateConfiguration;
    }

    public static EntityListTemplateConfiguration getEntityTemplateConfigurationValue(int templateConfigurationId) {
        return Arrays.stream(values()).filter(type -> type.value == templateConfigurationId).findFirst().orElse(getDefaultConfiguration());
    }

    public static EntityListTemplateConfiguration getDefaultConfiguration()
    {
        return EntityListTemplateConfiguration.FollowUnfollow;
    }
}
