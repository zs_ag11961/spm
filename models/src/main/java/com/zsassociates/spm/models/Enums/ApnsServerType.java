package com.zsassociates.spm.models.Enums;

public enum ApnsServerType {
    development,
    production,
    qa
}
