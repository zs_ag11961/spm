package com.zsassociates.spm.models.SystemConfiguration;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zsassociates.spm.models.BaseEntity;

import java.util.Date;

@JsonIgnoreProperties({"id", "data"})
public class SystemImage extends BaseEntity {

    private String Name;

    private String Extention;

    private long Size;


    private Date LastUpdatedTime;

    private String LastUpdatedBy;

    private byte[] Data;

    public SystemImage(long id, String name, String extention, long size, Date lastUpdatedTime, String lastUpdatedBy, byte[] data) {
        super(id);
        Name = name;
        Extention = extention;
        Size = size;
        LastUpdatedTime = lastUpdatedTime;
        LastUpdatedBy = lastUpdatedBy;
        Data = data;
        selfUrl = createSelfURL();
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getExtention() {
        return Extention;
    }

    public void setExtention(String extention) {
        Extention = extention;
    }

    public long getSize() {
        return Size;
    }

    public void setSize(long size) {
        Size = size;
    }

    public Date getLastUpdatedTime() {
        return LastUpdatedTime;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime) {
        LastUpdatedTime = lastUpdatedTime;
    }

    public String getLastUpdatedBy() {
        return LastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        LastUpdatedBy = lastUpdatedBy;
    }

    public byte[] getData() {
        return Data;
    }

    public void setData(byte[] data) {
        Data = data;
    }

    @Override
    public String createSelfURL() {
        return String.format("%s/%s/%s/%s", getBaseUrl(), "configuration", "image", getName());
    }
}
