package com.zsassociates.spm.models.SystemConfiguration;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zsassociates.spm.models.BaseEntity;

import java.util.Date;

@JsonIgnoreProperties({"selfUrl", "id"})
public class PaginationDetail extends BaseEntity {

    private String entity;

    private long pageSize;

    private long maxDisplayRecords;

    private Date LastUpdatedTime;

    private String LastUpdatedBy;

    public PaginationDetail(String entity, long pageSize, long maxDisplayRecords, Date LastUpdatedTime, String LastUpdatedBy) {
        this.entity = entity;
        this.pageSize = pageSize;
        this.maxDisplayRecords = maxDisplayRecords;
        this.LastUpdatedBy = LastUpdatedBy;
        this.LastUpdatedTime = LastUpdatedTime;
    }

    public PaginationDetail() {

    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public void setMaxDisplayRecords(long maxDisplayRecords) {
        this.maxDisplayRecords = maxDisplayRecords;
    }

    public String getEntity() {
        return entity;
    }

    public long getPageSize() {
        return pageSize;
    }

    public long getMaxDisplayRecords() {
        return maxDisplayRecords;
    }

    public Date getLastUpdatedTime() {
        return LastUpdatedTime;
    }

    public String getLastUpdatedBy() {
        return LastUpdatedBy;
    }

    @Override
    public String createSelfURL() {
        return null;
    }
}
