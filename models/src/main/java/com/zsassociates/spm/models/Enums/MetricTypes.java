package com.zsassociates.spm.models.Enums;

public enum MetricTypes {
    BASIC,
    DERIVED,
    COMPLEXDERIVED,
    TEXT
}
