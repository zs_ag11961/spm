package com.zsassociates.spm.handlers.IHandlers;

import com.zsassociates.spm.models.BaseEntity;

import java.util.List;

public interface IHandler<T extends BaseEntity> {

    long create(T entity);

    int update(T entity);

    List<T> selectAll();

    T getById(long id);

   // List<T> getFilteredList(Filter filter) throws Exception;
}
