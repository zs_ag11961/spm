package com.zsassociates.spm.handlers.Handlers;


import com.zsassociates.spm.dao.*;
import com.zsassociates.spm.handlers.IHandlers.IHandler;
import com.zsassociates.spm.models.BaseEntity;

import javax.inject.Inject;
import java.util.List;


public abstract class BaseHandler<T extends BaseEntity> implements IHandler<T> {

    private final IDAO<T> entityIdao;

    @Inject
    public BaseHandler(IDAO<T> entityIdao) {

        this.entityIdao = entityIdao;
    }

    public long create(T entity) {
        return entityIdao.create(entity);
    }

    public int update(T entity) {
        return entityIdao.update(entity);
    }

    public List<T> selectAll() {
        return entityIdao.selectAll();
    }

    public abstract T getById(long id);




}
