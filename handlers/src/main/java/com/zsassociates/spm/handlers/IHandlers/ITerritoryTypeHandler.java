package com.zsassociates.spm.handlers.IHandlers;

import com.zsassociates.spm.models.TerritoryType;

import java.util.List;

public interface ITerritoryTypeHandler extends IHandler<TerritoryType> {
    List<TerritoryType> getTerritories(long userId);

}
