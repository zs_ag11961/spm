package com.zsassociates.spm.handlers.Handlers;

import com.zsassociates.spm.IDao.ITerritoryTypeDao;
import com.zsassociates.spm.handlers.IHandlers.ITerritoryTypeHandler;
import com.zsassociates.spm.handlers.specs.Specs;
import com.zsassociates.spm.handlers.specs.TerritoryTypeSpecs;
import com.zsassociates.spm.models.TerritoryType;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TerritoryTypeHandler extends BaseHandler<TerritoryType> implements ITerritoryTypeHandler {

    private ITerritoryTypeDao territoryTypeDao;


    @Inject
    public TerritoryTypeHandler(ITerritoryTypeDao territoryTypeDao) {
        super(territoryTypeDao);
        this.territoryTypeDao = territoryTypeDao;
    }


    @Override
    public TerritoryType getById(long id) {
        return null;
    }

    @Override
    public List<TerritoryType> getTerritories(long territoryTypeId) {
        Specs.BaseSpec spec = new TerritoryTypeSpecs.GetTerritoryTypes();
        List<TerritoryType> allTerritories = territoryTypeDao.getMany (spec);
        return allTerritories;
    }
}

