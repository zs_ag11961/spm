//package com.zsassociates.spm.handlers;
//
//import com.zsassociates.aws.runtime.proxy.ApplicationUserIdProvider;
//import com.zsassociates.aws.runtime.proxy.GuiceInjectorFactory;
//import com.zsassociates.aws.runtime.proxy.JWTRequestFilter;
//import com.zsassociates.aws.runtime.proxy.JWTRequestFilter.PgConnectionSessionVariablesProvider;
//import org.apache.commons.lang3.StringUtils;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//
//import java.util.HashMap;
//import java.util.Map;
//
//public class PGSessionProvider implements PgConnectionSessionVariablesProvider {
//
//    private static final Logger log = LogManager.getLogger(PGSessionProvider.class);
//    @Override
//    public Map<String, String> getConnectionSessionVariables(JWTRequestFilter.JWTPrincipal jwtPrincipal) {
//        Map<String, String> connectionSessionVariables = new HashMap<>();
//
//        String consumerAppKey = jwtPrincipal.getApplicationKey();
//        ApplicationUserIdProvider userProvider = GuiceInjectorFactory.getInjector().getInstance(ApplicationUserIdProvider.class);
//
//        String applicationUserId = String.valueOf(userProvider.getUserByName(jwtPrincipal.getUserName().toLowerCase()));
//        // String applicationUserId = jwtPrincipal.getApplicationUserId();
//        if (StringUtils.isBlank(consumerAppKey))
//            return connectionSessionVariables;
//        //connectionSessionVariables.put("os.application_key", consumerAppKey);
//        connectionSessionVariables.put("os.user_id", applicationUserId);
//        return connectionSessionVariables;
//    }
//
//    @Override
//    public Map<String, String> getDefaultSessionVariables() {
//        Map<String, String> connectionSessionVariables = new HashMap<>();
//        connectionSessionVariables.put("os.user_id", "1");
//        connectionSessionVariables.put("os.application_key", "NotValidKey");
//        connectionSessionVariables.put("os.rep_id", "-1");
//        return connectionSessionVariables;
//    }
//}
