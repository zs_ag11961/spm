package com.zsassociates.spm.exceptions;

import com.zsassociates.core.model.ValidationError;
import com.zsassociates.core.model.ValidationErrorCollection;
import com.zsassociates.core.model.ValidationException;

import java.util.List;

public abstract class InsightValidationException extends ValidationException {

	public int httpStatusCode;
	
    public abstract int getHttpStatusCode();
    
    public abstract void setHttpStatusCode(int httpStatusCode);
    
    public InsightValidationException(ValidationError validationError) {
        super(validationError);
    }

    public InsightValidationException(ValidationErrorCollection validationErrorCollection) {
        super(validationErrorCollection);
    }

    public InsightValidationException(List<ValidationError> errors) {
        this(new ValidationErrorCollection(errors));
    }

    public InsightValidationException(long errorCode, String message) {
        super(new ValidationError(errorCode, message));
    }

    public InsightValidationException(String message) {
        super(message);
    }
    
    
    //additional constructor for http status code error
    public InsightValidationException(ValidationErrorCollection validationErrorCollection, int httpStatusCode) {
        super(validationErrorCollection, httpStatusCode);
        this.httpStatusCode = httpStatusCode;
    }    
    
    public InsightValidationException(List<ValidationError> errors, int httpStatusCode) {
        this(new ValidationErrorCollection(errors), httpStatusCode);
        this.httpStatusCode = httpStatusCode;
    }   
    
    public InsightValidationException(long errorCode, String message, int httpStatusCode) {
        super(new ValidationError(errorCode, message), httpStatusCode);
        this.httpStatusCode = httpStatusCode;
    }

    public InsightValidationException(String message, int httpStatusCode) {
        super(message, httpStatusCode);
        this.httpStatusCode = httpStatusCode;
    }
    
}

