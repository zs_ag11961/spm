package com.zsassociates.spm.exceptions;

import com.zsassociates.core.model.ValidationError;
import com.zsassociates.core.model.ValidationErrorCollection;
import org.apache.http.HttpStatus;

import java.io.Serializable;
import java.util.List;

/**
 * Base Exception for NotImplemented
 * @author db18651
 */
public class NotImplementedException extends InsightValidationException implements Serializable{

	private static final long serialVersionUID = 1L;
	private static int HTTP_STATUS_CODE = HttpStatus.SC_NOT_IMPLEMENTED;

    public NotImplementedException(ValidationErrorCollection validationErrorCollection) {
		super(validationErrorCollection, NotImplementedException.HTTP_STATUS_CODE);
	}

    public NotImplementedException(List<ValidationError> errors) {
		super(errors, NotImplementedException.HTTP_STATUS_CODE);
	}

    public NotImplementedException(long errorCode, String message) {
        super(errorCode, message, NotImplementedException.HTTP_STATUS_CODE);
    }

    public NotImplementedException(String message) {
        super(message, NotImplementedException.HTTP_STATUS_CODE);
    }

	@Override
	public int getHttpStatusCode() {
		return HTTP_STATUS_CODE;
	}

	@Override
	public void setHttpStatusCode(int httpStatusCode) {
		
	}
}
