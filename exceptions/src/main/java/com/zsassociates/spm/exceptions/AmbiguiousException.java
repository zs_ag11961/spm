package com.zsassociates.spm.exceptions;

import com.zsassociates.core.model.ValidationError;
import com.zsassociates.core.model.ValidationErrorCollection;
import org.apache.http.HttpStatus;

import java.io.Serializable;
import java.util.List;

/**
 * Base Exception for Bad Request
 *
 * @author rr19320
 */
public class AmbiguiousException extends InsightValidationException implements Serializable {

	private static final long serialVersionUID = 1L;

	private static int HTTP_STATUS_CODE = HttpStatus.SC_BAD_REQUEST;

	private Object ambiguousObject;

    public AmbiguiousException(ValidationErrorCollection validationErrorCollection) {
		super(validationErrorCollection, AmbiguiousException.HTTP_STATUS_CODE);
	}

    public AmbiguiousException(List<ValidationError> errors) {
		super(errors, AmbiguiousException.HTTP_STATUS_CODE);
	}

    public AmbiguiousException(long errorCode, String message) {
        super(errorCode, message, AmbiguiousException.HTTP_STATUS_CODE);
    }

    public AmbiguiousException(String message,Object o)
	{
		super(message, AmbiguiousException.HTTP_STATUS_CODE);
		this.ambiguousObject = o;
	}

    public AmbiguiousException(String message) {
        super(message, AmbiguiousException.HTTP_STATUS_CODE);
    }

	@Override
	public int getHttpStatusCode() {
		return HTTP_STATUS_CODE;
	}

	@Override
	public void setHttpStatusCode(int httpStatusCode) {
	}

	public Object getAmbiguousObject() {
		return ambiguousObject;
	}

}
