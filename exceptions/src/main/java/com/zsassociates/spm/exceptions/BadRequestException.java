package com.zsassociates.spm.exceptions;

import com.zsassociates.core.model.ValidationError;
import com.zsassociates.core.model.ValidationErrorCollection;
import org.apache.http.HttpStatus;

import java.io.Serializable;
import java.util.List;

/**
 * Base Exception for Bad Request
 *
 * @author rr19320
 */
public class BadRequestException extends InsightValidationException implements Serializable {

	private static final long serialVersionUID = 1L;

	private static int HTTP_STATUS_CODE = HttpStatus.SC_BAD_REQUEST;

    public BadRequestException(ValidationErrorCollection validationErrorCollection) {
		super(validationErrorCollection, BadRequestException.HTTP_STATUS_CODE);
	}

    public BadRequestException(List<ValidationError> errors) {
		super(errors, BadRequestException.HTTP_STATUS_CODE);
	}

    public BadRequestException(long errorCode, String message) {
        super(errorCode, message, BadRequestException.HTTP_STATUS_CODE);
    }

    public BadRequestException(String message) {
        super(message, BadRequestException.HTTP_STATUS_CODE);
    }

	@Override
	public int getHttpStatusCode() {
		return HTTP_STATUS_CODE;
	}

	@Override
	public void setHttpStatusCode(int httpStatusCode) {
	}
}
