package com.zsassociates.spm.exceptions.enums;

public enum Exceptions {

	BadRequestException,
	UnAuhorizedException,
	ForbiddenException,
	NotFoundException,
	ConflictException,
	InternalServerException,
	BadGatewayException,
	ServiceUnavailableException,
	GatewayTimeoutException,
	NotImplemetedException
}
