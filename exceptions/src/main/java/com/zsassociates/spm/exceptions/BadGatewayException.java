package com.zsassociates.spm.exceptions;

import com.zsassociates.core.model.ValidationError;
import com.zsassociates.core.model.ValidationErrorCollection;
import org.apache.http.HttpStatus;

import java.io.Serializable;
import java.util.List;

/**
 * Base Exception for Bad gateway
 * @author rr19320
 */
public class BadGatewayException extends InsightValidationException implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static int HTTP_STATUS_CODE = HttpStatus.SC_BAD_GATEWAY;
	
    public BadGatewayException(ValidationErrorCollection validationErrorCollection) {
		super(validationErrorCollection, BadGatewayException.HTTP_STATUS_CODE);
	}
    
    public BadGatewayException(List<ValidationError> errors) {
		super(errors, BadGatewayException.HTTP_STATUS_CODE);
	}

    public BadGatewayException(long errorCode, String message) {
        super(errorCode, message, BadGatewayException.HTTP_STATUS_CODE);
    }
 	
    public BadGatewayException(String message) {
        super(message, BadGatewayException.HTTP_STATUS_CODE);
    }
    
	@Override
	public int getHttpStatusCode() {
		return HTTP_STATUS_CODE;
	}

	@Override
	public void setHttpStatusCode(int httpStatusCode) {
		
	}

}
