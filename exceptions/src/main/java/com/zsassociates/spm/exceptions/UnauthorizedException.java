package com.zsassociates.spm.exceptions;

import com.zsassociates.core.model.ValidationError;
import com.zsassociates.core.model.ValidationErrorCollection;
import org.apache.http.HttpStatus;

import java.io.Serializable;
import java.util.List;

/**
 * Base Exception for Unauthorized
 * @author rr19320
 */
public class UnauthorizedException extends InsightValidationException implements Serializable{

	private static final long serialVersionUID = 1L;
	private static int HTTP_STATUS_CODE = HttpStatus.SC_UNAUTHORIZED;
	
    public UnauthorizedException(ValidationErrorCollection validationErrorCollection) {
		super(validationErrorCollection, UnauthorizedException.HTTP_STATUS_CODE);
	}
    
    public UnauthorizedException(List<ValidationError> errors) {
		super(errors, UnauthorizedException.HTTP_STATUS_CODE);
	}

    public UnauthorizedException(long errorCode, String message) {
        super(errorCode, message, UnauthorizedException.HTTP_STATUS_CODE);
    }
 	
    public UnauthorizedException(String message) {
        super(message, UnauthorizedException.HTTP_STATUS_CODE);
    }

	@Override
	public int getHttpStatusCode() {
		return HTTP_STATUS_CODE;
	}

	@Override
	public void setHttpStatusCode(int httpStatusCode) {
		
	}
}
