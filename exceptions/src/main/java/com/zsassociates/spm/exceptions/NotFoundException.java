package com.zsassociates.spm.exceptions;

import com.zsassociates.core.model.ValidationError;
import com.zsassociates.core.model.ValidationErrorCollection;
import org.apache.http.HttpStatus;

import java.io.Serializable;
import java.util.List;

/**
 * Base Exception for NotFound
 * @author rr19320
 */
public class NotFoundException extends InsightValidationException implements Serializable{

	private static final long serialVersionUID = 1L;
	private static int HTTP_STATUS_CODE = HttpStatus.SC_NOT_FOUND;
	
    public NotFoundException(ValidationErrorCollection validationErrorCollection) {
		super(validationErrorCollection, NotFoundException.HTTP_STATUS_CODE);
	}
    
    public NotFoundException(List<ValidationError> errors) {
		super(errors, NotFoundException.HTTP_STATUS_CODE);
	}

    public NotFoundException(long errorCode, String message) {
        super(errorCode, message, NotFoundException.HTTP_STATUS_CODE);
    }
 	
    public NotFoundException(String message) {
        super(message, NotFoundException.HTTP_STATUS_CODE);
    }

	@Override
	public int getHttpStatusCode() {
		return HTTP_STATUS_CODE;
	}

	@Override
	public void setHttpStatusCode(int httpStatusCode) {
		
	}
}
