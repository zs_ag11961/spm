package com.zsassociates.spm.exceptions.handler;

import com.zsassociates.core.model.ValidationException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ZSBaseExceptionHandler implements ExceptionMapper<ValidationException> {

	@Override
	public Response toResponse(ValidationException exception) {
		
		return Response.
				status(exception.getHttpStatusCode()).entity(exception.toString()
						).type(MediaType.APPLICATION_JSON).build();
	}

}
