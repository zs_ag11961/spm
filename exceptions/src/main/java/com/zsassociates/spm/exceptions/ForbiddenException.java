package com.zsassociates.spm.exceptions;

import com.zsassociates.core.model.ValidationError;
import com.zsassociates.core.model.ValidationErrorCollection;
import org.apache.http.HttpStatus;

import java.io.Serializable;
import java.util.List;

/**
 * Base Exception for Forbidden
 * @author rr19320
 */
public class ForbiddenException extends InsightValidationException implements Serializable{

	private static final long serialVersionUID = 1L;
	private static int HTTP_STATUS_CODE = HttpStatus.SC_FORBIDDEN;
	
    public ForbiddenException(ValidationErrorCollection validationErrorCollection) {
		super(validationErrorCollection, ForbiddenException.HTTP_STATUS_CODE);
	}
    
    public ForbiddenException(List<ValidationError> errors) {
		super(errors, ForbiddenException.HTTP_STATUS_CODE);
	}

    public ForbiddenException(long errorCode, String message) {
        super(errorCode, message, ForbiddenException.HTTP_STATUS_CODE);
    }
 	
    public ForbiddenException(String message) {
        super(message, ForbiddenException.HTTP_STATUS_CODE);
    }

	@Override
	public int getHttpStatusCode() {
		return HTTP_STATUS_CODE;
	}

	@Override
	public void setHttpStatusCode(int httpStatusCode) {
		
	}
}
