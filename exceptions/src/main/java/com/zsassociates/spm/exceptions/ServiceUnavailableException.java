package com.zsassociates.spm.exceptions;

import com.zsassociates.core.model.ValidationError;
import com.zsassociates.core.model.ValidationErrorCollection;
import org.apache.http.HttpStatus;

import java.io.Serializable;
import java.util.List;

/**
 * Base Exception for Service Unavailable
 * @author rr19320
 */
public class ServiceUnavailableException extends InsightValidationException implements Serializable  {

	private static final long serialVersionUID = 1L;
	static int HTTP_STATUS_CODE = HttpStatus.SC_SERVICE_UNAVAILABLE;
	
    public ServiceUnavailableException(ValidationErrorCollection validationErrorCollection) {
		super(validationErrorCollection, ServiceUnavailableException.HTTP_STATUS_CODE);
	}
    
    public ServiceUnavailableException(List<ValidationError> errors) {
		super(errors, ServiceUnavailableException.HTTP_STATUS_CODE);
	}

    public ServiceUnavailableException(long errorCode, String message) {
        super(errorCode, message, ServiceUnavailableException.HTTP_STATUS_CODE);
    }
 	
    public ServiceUnavailableException(String message) {
        super(message, ServiceUnavailableException.HTTP_STATUS_CODE);
    }

	
	@Override
	public int getHttpStatusCode() {
		return HTTP_STATUS_CODE;
	}

	@Override
	public void setHttpStatusCode(int httpStatusCode) {
		
	}

}
